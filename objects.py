import pyglet, random

batch = pyglet.graphics.Batch()
field_batch = pyglet.graphics.Batch()
highligh_batch = pyglet.graphics.Batch()
label_batch = pyglet.graphics.Batch()
fields = pyglet.graphics.OrderedGroup(0)
resources = pyglet.graphics.OrderedGroup(1)
buildings = pyglet.graphics.OrderedGroup(2)
field_highlight = pyglet.graphics.OrderedGroup(3)
foreground = pyglet.graphics.OrderedGroup(4)
button_labels = pyglet.graphics.OrderedGroup(5)

field_types = ["Empty", "Forest", "Meadow", "Water", "Mountain"]

colors =	{"green": (0,255,0),
			"red": (255,0,0),
			"blue": (0,0,255)}
			
pyglet.resource.path = ["images", "images/buildings", "images/buttons", "images/fields", "images/other", "images/resources"]
pyglet.resource.reindex()

##### GENERAL OBJECTS ######
class GeneralObject(object):
	x = 0
	y = 0
	width = 0
	height = 0
	name = None
	type = None
	subtype = None
	sprite = None
	game_object = None
	parent = None
	children = []
	selected = False
	highlight_object = None
	
	def __init__(self, x, y, game_object):
		self.x = x
		self.y = y
		self.game_object = game_object
		
	def change_position(self, shift_x, shift_y):
		self.x -= shift_x
		self.y -= shift_y
		if self.sprite is not None:
			self.sprite.x = self.x
			self.sprite.y = self.y
			for child in self.children:
				child.change_position(shift_x, shift_y)
	
	def start_highlight(self, color, group):
		self.highlight_object = Highlight(self, color, group)
		self.children.append(self.highlight_object)
	
	def stop_highlight(self):
		self.highlight_object.stop()
		self.children.remove(self.highlight_object)
		self.highlight_object = None
	
	def is_highlighted(self):
		if self in Field.highlighted_fields:
			return True
		else:
			return False
	
class SelectableObject(GeneralObject):
	def select(self):
		if self.selected:
			self.deselect()
		else:
			self.selected = True
			self.start_highlight(colors["blue"], button_labels)
	
	def deselect(self):
		self.selected = False
		self.stop_highlight()



##### HIGHLIGHTS #####
class Highlight(object):
	image = pyglet.resource.image("highlight.png")
	
	def __init__(self, parent, color, group):
		self.parent = parent
		self.sprite = pyglet.sprite.Sprite(Highlight.image, self.parent.x, self.parent.y, batch=batch, group=group)
		self.sprite.opacity = 200
		self.sprite.color = color
		self.type = "highlight"
	
	def stop(self):
		self.sprite.delete()
		del self
	
	def change_position(self, shift_x, shift_y):
		self.sprite.x = self.parent.x
		self.sprite.y = self.parent.y


##### LABELS #####
class ResourceLabel(pyglet.text.Label):
	def __init__(self, text, size, x, y, color, bold):
		super(ResourceLabel, self).__init__(text,
											font_name="Times New Roman",
											font_size=size,
											x=x, y=y,
											anchor_x="left", anchor_y="bottom",
											color=color,
											bold=bold,
											batch=batch,
											group=button_labels)
		self.type = "label"
	
	def change_position(self, shift_x, shift_y):
		self.x -= shift_x
		self.y -= shift_y


##### SELECTION AREA #####
class SelectionArea(object):
	def __init__(self):
		self.size = 1
	
	def change_size(self):
		if self.size == 1:
			self.size = 9
		elif self.size == 9:
			self.size = 1
	
	def get_size(self):
		return self.size

##### SELECTED OBJECTS #####
class SelectedObjects(object):
	def __init__(self):
		self.objects = []
	
	def select_object(self, object):
		self.object.append(object)
	
	def deselect_object(self, object):
		self.object.remove(object)
	
	def get_objects(self):
		return self.objects


##### BUTTONS #####
class Button(SelectableObject):
	def __init__(self, x, y, game_object):
		super(Button, self).__init__(x, y, game_object)
		self.type = "button"
	
	def set_image_sprite(self, image_name):
		self.image = pyglet.resource.image(image_name)
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch, group=button_labels)
		self.height = self.image.height
		self.width = self.image.width
	
	def action(self, target):
		if target.type == "button":
			self.game_object.select_item(target)
		elif target.type == "field":
			if self.name == "Wood Factory":
				building = Factory_Wood(self.game_object, target)
			elif self.name == "Stone Factory":
				building = Factory_Stone(self.game_object, target)
			elif self.name == "Iron Factory":
				building = Factory_Iron(self.game_object, target)
		else:
			pass

class ButtonBuildingFactoryWood(Button):
	def __init__(self, x, y, game_object):
		super(ButtonBuildingFactoryWood, self).__init__(x, y, game_object)
		self.name = "Wood Factory"
		self.subtype = "factory"
		self.set_image_sprite("button_factory_wood.png")

class ButtonBuildingFactoryStone(Button):
	def __init__(self, x, y, game_object):
		super(ButtonBuildingFactoryStone, self).__init__(x, y, game_object)
		self.name = "Stone Factory"
		self.subtype = "factory"
		self.set_image_sprite("button_factory_stone.png")

class ButtonBuildingFactoryIron(Button):
	def __init__(self, x, y, game_object):
		super(ButtonBuildingFactoryIron, self).__init__(x, y, game_object)
		self.name = "Iron Factory"
		self.subtype = "factory"
		self.set_image_sprite("button_factory_iron.png")


##### FIELD TYPES #####
class Field(GeneralObject):
	column = 0
	row = 0
	highlighted_fields = []
	building_present = None
	
	wood = None
	stone = None
	food = None
	iron = None
	
	def __init__(self, x, y, game_object):
		super(Field, self).__init__(x, y, game_object)
		self.height = self.game_object.field_size
		self.width = self.game_object.field_size
		self.type = "field"
	
	def build_building(self, building):
		self.building_present = building

	
	def destroy_building(self):
		self.building_present = None
	
	def set_image_sprite(self, image_name):
		self.image = pyglet.resource.image(image_name)
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch, group=fields)
	
	def set_resources(self):
		self.resources =	{"Wood": Wood(self),
							"Stone": Stone(self),
							"Food": Food(self),
							"Iron": Iron(self)}

	def highlight(self, area_fields):
		if len(Field.highlighted_fields) != 0:
			if area_fields != Field.highlighted_fields:
				for field in Field.highlighted_fields:
					field.stop_highlight()

				Field.highlighted_fields = area_fields
				
				for field in Field.highlighted_fields:
					field.start_highlight(colors["green"], field_highlight)
		else:
			self.start_highlight(colors["green"], field_highlight)
			Field.highlighted_fields = area_fields

	def show_resources(self):
		return [self.resources["Wood"].amount, self.resources["Stone"].amount, self.resources["Iron"].amount, self.resources["Food"].amount]
	
	def update_resource(self, resource, change):
		self.resources[resource].amount += change

class Empty(Field):
	def __init__(self, x, y, game_object):
		super(Empty, self).__init__(x, y, game_object)
		self.name = "Empty"
		self.set_image_sprite("field_empty.png")
		#self.set_height_width(self.image)
		self.set_resources()

class Forest(Field):
	def __init__(self, x, y, game_object):
		super(Forest, self).__init__(x, y, game_object)
		self.name = "Forest"
		self.set_image_sprite("field_forest.png")
		#self.set_height_width(self.image)
		self.set_resources()

class Meadow(Field):
	def __init__(self, x, y, game_object):
		super(Meadow, self).__init__(x, y, game_object)
		self.name = "Meadow"
		self.set_image_sprite("field_meadow.png")
		#self.set_height_width(self.image)
		self.set_resources()

class Mountain(Field):
	def __init__(self, x, y, game_object):
		super(Mountain, self).__init__(x, y, game_object)
		self.name = "Mountain"
		self.set_image_sprite("field_mountain.png")
		#self.set_height_width(self.image)
		self.set_resources()

class Water(Field):
	def __init__(self, x, y, game_object):
		super(Water, self).__init__(x, y, game_object)
		self.name = "Water"
		self.set_image_sprite("field_water.png")
		#self.set_height_width(self.image)
		self.set_resources()

##### RESOURCE TYPES #####
class Resource(object):
	type = "resource"
	name = None
	parent_field = None
	amount = 0
	amount_range = {"Empty":(0,0),
					"Forest":(0,0),
					"Meadow":(0,0),
					"Mountain":(0,0),
					"Water":(0,0),
					"NoneType":(0,0)}
	image = None
	image_grid_count = 4
	image_grid = None
	image_grid_frame = None
	
	def __init__(self, field):
		self.parent_field = field
		self.set_amount(self.parent_field.name)
	
	def set_amount(self, field_type):
		if self.amount_range[field_type] != (0,0):
			self.amount = random.randrange(*self.amount_range[field_type])
		else:
			self.amount = 0
	
	def set_image_sprite(self, image_name):
		self.image = pyglet.resource.image(image_name)
		self.image_grid = pyglet.image.ImageGrid(self.image, 1, self.image_grid_count)
		if self.amount > 8 and self.amount < 25:
			self.image_grid_frame = self.image_grid[0]
		elif self.amount >= 25 and self.amount < 50:
			self.image_grid_frame = self.image_grid[1]
		elif self.amount >= 50 and self.amount < 75:
			self.image_grid_frame = self.image_grid[2]
		elif self.amount >= 75 and self.amount <= 100:
			self.image_grid_frame = self.image_grid[3]
		
		if self.image_grid_frame:
			self.sprite = pyglet.sprite.Sprite(self.image_grid_frame, self.parent_field.x, self.parent_field.y, batch=batch, group=resources)
	
#	def harvest_resource(self, change):
#		self.amount -= change
#	
#	def replenish_resource(self, change):
#		self.amount += change

class Wood(Resource):
	def __init__(self, field):
		self.amount_range["Empty"] = (0, 10)
		self.amount_range["Forest"] = (70, 100)
		self.amount_range["Meadow"] = (0, 0)
		self.amount_range["Mountain"] = (25,75)
		self.amount_range["Water"] = (0, 0)
		
		super(Wood, self).__init__(field)
		self.name = "Wood"		
		self.set_image_sprite("res_wood.png")

class Stone(Resource):
	def __init__(self, field):
		self.amount_range["Empty"] = (0, 10)
		self.amount_range["Forest"] = (10, 40)
		self.amount_range["Meadow"] = (0, 0)
		self.amount_range["Mountain"] = (75,100)
		self.amount_range["Water"] = (0, 0)
		
		super(Stone, self).__init__(field)
		self.name = "Stone"
		self.set_image_sprite("res_stone.png")

class Iron(Resource):
	def __init__(self, field):
		self.amount_range["Empty"] = (0, 0)
		self.amount_range["Forest"] = (0, 0)
		self.amount_range["Meadow"] = (0, 0)
		self.amount_range["Mountain"] = (50,100)
		self.amount_range["Water"] = (0, 0)
		
		super(Iron, self).__init__(field)
		self.name = "Iron"
		self.set_image_sprite("res_iron.png")

class Food(Resource):
	def __init__(self, field):
		self.amount_range["Empty"] = (0, 0)
		self.amount_range["Forest"] = (0, 25)
		self.amount_range["Meadow"] = (20, 70)
		self.amount_range["Mountain"] = (0,0)
		self.amount_range["Water"] = (0, 30)
		
		super(Food, self).__init__(field)
		self.name = "Food"
		self.set_image_sprite("res_food.png")

class ResourceBuilding(object):
	#general info
	name = None
	type = "resourceBuilding"
	parent_field = None
	game_object = None
	
	#visuals
	image = None
	sprite = None
	
	#selection info
	selected = False
	highlight_object = None
	
	#stats
	health = 0
	resource = None
	production = 0.0
	stopped = False
	
	def __init__(self, game_object, field):
		self.game_object = game_object
		self.parent_field = field
		self.x = self.parent_field.x
		self.y = self.parent_field.y
		self.game_object.add_to_selectable_objects(self)
		self.game_object.add_to_resource_buildings(self)
		self.parent_field.build_building(self)
	
	def set_image_sprite(self, image_name):
		self.image = pyglet.resource.image(image_name)
		self.sprite = pyglet.sprite.Sprite(self.image, self.parent_field.x, self.parent_field.y, batch=batch, group=buildings)
	
	def harvest_resources(self, amount):
		self.parent_field.resources[self.resource].amount -= amount
	
	def update_production(self):
		if self.parent_field.resources[self.resource].amount > 0:
			pass
		else:
			self.production = 0.0

	def select(self):
		if self.selected:
			self.deselect()
		else:
			self.selected = True
			self.start_highlight(colors["blue"], field_highlight)
	
	def deselect(self):
		self.selected = False
		self.stop_highlight()
	
	def action(self, target):
		if target.type != "field":
			self.game_object.select_item(target)
		else:
			pass
	
	def start_highlight(self, color, group):
		self.highlight_object = Highlight(self, color, group)
	
	def stop_highlight(self):
		self.highlight_object.stop()
		self.highlight_object = None


class Factory_Wood(ResourceBuilding):
	def __init__(self, game_object, field):
		super(Factory_Wood, self).__init__(game_object, field)
		self.name = "Wood Factory"
		self.resource = "Wood"
		#production per minute, vazane na resource_update_clock z game
		self.production = 10.0
		self.set_image_sprite("factory_wood.png")

class Factory_Stone(ResourceBuilding):
	def __init__(self, game_object, field):
		super(Factory_Stone, self).__init__(game_object, field)
		self.name = "Stone Factory"
		self.resource = "Stone"
		self.production = 10.0
		self.set_image_sprite("factory_stone.png")

class Factory_Iron(ResourceBuilding):
	def __init__(self, game_object, field):
		super(Factory_Iron, self).__init__(game_object, field)
		self.name = "Stone Factory"
		self.resource = "Iron"
		self.production = 10.0
		self.set_image_sprite("factory_iron.png")

#class Vehicle(object):

