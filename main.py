import pyglet, pyglet.gl
from game import *

# + generovani fieldu s ruznym typem surovin
# + highlight ukazatele mysi
# + zobrazeni mnozstvi surovin
# + zobrazovani mnozstvi surovin ve vetsim vyberu
# + spravne pricitani resources
# - vetsi plocha
# - pohyb po vetsi plose
# - dalsi objekty jako budovy (tyhle maji zdravi)
# !!! - kontrola a uprava objektu, aby se k jejich vlastnostem pristupovalo jen pres funkce (zapouzdreni)
# - jednotna templata objektu (se vsemi funkcemi co objekty muzou mit)
# - zalozky v menu aree pro budovy, vozitka atd
# - info menu o vybrane budove, vozitku atd
# - vymyslet lepsi odecitani/casovani/production resourcu
#

class GameWindow(pyglet.window.Window):
	def __init__(self):
		super(GameWindow, self).__init__(1000,600)
		self.shift_x_total = 0
		self.shift_y_total = 0
		self.shift_x = 0
		self.shift_y = 0
		self.game = Game(self.height, self.width)
		self.fps_display = pyglet.clock.ClockDisplay()
		
		pyglet.gl.glEnable(pyglet.gl.GL_TEXTURE_2D)
		pyglet.gl.glTexParameteri(pyglet.gl.GL_TEXTURE_2D, pyglet.gl.GL_TEXTURE_MAG_FILTER, pyglet.gl.GL_NEAREST)

	def on_draw(self):
		self.clear()
		#field_batch.draw()
		#highligh_batch.draw()
		#label_batch.draw()
		batch.draw()
		
		self.fps_display.draw()
		
	def on_mouse_motion(self, x, y, dx, dy):
		#fields = self.game.get_fields(x, y, self.shift_x_total, self.shift_y_total)
		object = self.game.get_object(x, y, self.shift_x_total, self.shift_y_total)
		if object is not None:
			if object.type == "field":
				#object.highlight([object])
				#print fields
				self.game.update_selected_fields_highligh((object, [object]))
				self.game.show_selected_fields_resources((object, [object]))
			elif object.type == "resourceBuilding":
				self.game.update_selected_fields_highligh((object.parent_field, [object.parent_field]))
				self.game.show_selected_fields_resources((object.parent_field, [object.parent_field]))
				#object.parent_field.highlight([object.parent_field])
	
	def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
		if buttons == pyglet.window.mouse.MIDDLE:
			self.shift_x_total -= dx
			self.shift_y_total -= dy
			self.shift_x = dx
			self.shift_y = dy
			pyglet.gl.glTranslatef(dx,dy,0)
			self.game.compensate_shift_in_border(self.shift_x, self.shift_y)

	def on_mouse_press(self, x, y, button, modifiers):
		#if button == pyglet.window.mouse.MIDDLE:
		#	self.game.game_area.selection_area.change_size()
		if button == pyglet.window.mouse.LEFT:
			#print x, y
			clicked = self.game.get_object(x, y, self.shift_x_total, self.shift_y_total)
			if clicked is not None:
				if self.game.selected_item is None:
					self.game.select_item(clicked)
				else:
					self.game.selected_item.action(clicked)
		elif button == pyglet.window.mouse.RIGHT:
			self.game.deselect_all()

if __name__ == "__main__":
	window = GameWindow()
	pyglet.app.run()