#vic typu materialu
#vetsi plocha
#budovy
#suroviny + jejich pocitani
#pohyblive casti?
#textury ktere se meni podle jejich okoli


import pyglet, pyglet.gl, time, math

window_height = 500
window_width = 1000

window = pyglet.window.Window(window_width, window_height)

fps_display = pyglet.clock.ClockDisplay()

batch = pyglet.graphics.Batch()
background = pyglet.graphics.OrderedGroup(0)
foreground = pyglet.graphics.OrderedGroup(1)

field_types =	{"empty": pyglet.image.load("box_empty.png"),
				"ground": pyglet.image.load("box_ground.png"),
				"water": pyglet.image.load("box_water.png"),
				"forest": pyglet.image.load("box_forest.png")}

colors =	{"green": (0,255,0),
			"red": (255,0,0),
			"blue": (0,0,255)}
			
field_combinations =	{"empty": ["empty", "ground", "water", "forest"],
						"ground": ["ground", "forest", "empty"],
						"water": ["water", "empty"],
						"forest": ["forest", "empty"]}
				
keys_held = []
object_under = None


class GameArea():
	block_size = 50
	height = window_height * 0.9
	width = (window_width * 0.7) - ((window_width * 0.7) % block_size)
	anchor_x = window_width * 0.033
	anchor_y = window_height * 0.05
	rect = (anchor_x, anchor_y, anchor_x+width, anchor_y, anchor_x+width, anchor_y+height, anchor_x, anchor_y+height)
	
	def __init__(self):
		pyglet.gl.glColor3f(255,255,255)
		batch.add(4, pyglet.gl.GL_QUADS, background, ('v2f', GameArea.rect))
		self.create_grid()
	
	def create_grid(self):
		self.grid = Grid(GameArea.anchor_x, GameArea.anchor_y, GameArea.width, GameArea.height)
		self.grid.fill_grid()

class MenuArea():
	height = window_height * 0.9
	width = window_width * 0.2
	anchor_x = GameArea.width + (window_width * 0.066)
	anchor_y = window_height * 0.05
	rect = (anchor_x, anchor_y, anchor_x+width, anchor_y, anchor_x+width, anchor_y+height, anchor_x, anchor_y+height)
	
	def __init__(self):
		self.rect = MenuArea.rect
		pyglet.gl.glColor3f(0.82,0.94,0.47)
		batch.add(4, pyglet.gl.GL_QUADS, background, ('v2f', self.rect))
		self.add_buttons()
	
	def add_buttons(self):
		button_count = len(field_types)
		index = 0
		for type in field_types:
			index += 1
			Button(MenuArea.anchor_x + (MenuArea.width/2), MenuArea.anchor_y+((MenuArea.height/(button_count+1))*index), field_types[type], type)

class Field():
	fieldObjects = {}
	image = pyglet.image.load("box_empty.png")
	type = "empty"
	highlighted_field = None
	
	def __init__(self, x, y):
		Field.fieldObjects[id(self)] = self
		self.type = Field.type
		self.x = x
		self.y = y
		self.width = Field.image.width
		self.height = Field.image.height
		self.sprite = pyglet.sprite.Sprite(Field.image, self.x, self.y, batch=batch, group=background)
		self.highlight = None
	
	def select(self):
		if Button.selected:
			if Button.selected.type in field_combinations[self.type]:
				if self.type != Button.selected.type:
					self.type = Button.selected.type
					self.sprite.image = field_types[Button.selected.type]
	
	def start_highlight(self, color):
		Field.highlighted_field = self
		self.highlight = Highlight(self.x, self.y, color)
	
	def stop_highlight(self):
		self.highlight.stop()
		Field.highlighted_field = None
	
	def is_highlighted(self):
		if Field.highlighted_field is self:
			return True
	
	def check_highlight(self):
		if Button.selected.type in field_combinations[self.type]:
			self.highlight.sprite.color = (colors["green"])
		else:
			self.highlight.sprite.color = (colors["red"])

class Grid():
	def __init__(self, anchor_x, anchor_y, width, height):
		self.anchor_x = anchor_x
		self.anchor_y = anchor_y
		self.width = width
		self.height = height
		self.columns = math.floor(width / GameArea.block_size)
		self.rows = math.floor(height / GameArea.block_size)
	
	def fill_grid(self):
		for column in range(int(self.columns)):
			for row in range(int(self.rows)):
				Field(self.anchor_x+column*GameArea.block_size, self.anchor_y+row*GameArea.block_size)

class Button():
	buttonObjects = {}
	selected = None
	
	def __init__(self, x, y, image, type):
		Button.buttonObjects[id(self)] = self
		self.image = image
		self.type = type
		self.x = x-(self.image.width/2)
		self.y = y-(self.image.height/2)
		self.width = self.image.width
		self.height = self.image.height
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch, group=background)
		self.highlight = None
	
	def select(self):
		if Button.selected is not self:
			if Button.selected is not None:
				Button.selected.highlight.stop()
			Button.selected = self
			self.start_highlight(colors["blue"])
			if Field.highlighted_field:
				Field.highlighted_field.check_highlight()
		elif Button.selected is self:
			Button.selected = None
			self.stop_highlight()
			if Field.highlighted_field is not None:
				Field.highlighted_field.stop_highlight()
	
	def start_highlight(self, color):
		self.highlight = Highlight(self.x, self.y, color)
	
	def stop_highlight(self):
		self.highlight.stop()

class Highlight():
	image = pyglet.image.load("highlight.png")
	
	def __init__(self, x, y, color):
		self.sprite = pyglet.sprite.Sprite(Highlight.image, x, y, batch=batch, group=foreground)
		self.sprite.opacity = 200
		self.sprite.color = color
	def stop(self):
		self.sprite.delete()
		del self

class Border():
	type = "border"
	def select(self):
		pass
	def start_highlight(self, color):
		pass
	def stop_highlight(self):
		pass

def get_object(x, y):
	result = False
	if in_menuArea(x, y):
		result = check_buttons(x, y)
	elif in_gameArea(x, y):
		result = check_fields(x, y)
	else:
		result = border
	return result
	
def in_menuArea(x, y):
	if x >= MenuArea.anchor_x and x <= MenuArea.anchor_x + MenuArea.width:
		if y >= MenuArea.anchor_y and y <= MenuArea.anchor_y + MenuArea.height:
			return True
		
def in_gameArea(x, y):
	if x >= GameArea.anchor_x and x <= GameArea.anchor_x + GameArea.width:
		if y >= GameArea.anchor_y and y <= GameArea.anchor_y + GameArea.height:
			return True

def check_buttons(x, y):
	for button in Button.buttonObjects:
		button = Button.buttonObjects[button]
		if x >= button.x and x <= button.x + button.width:
			if y >= button.y and y <= button.y + button.height:
				return button
	return border

def check_fields(x, y):
	for field in Field.fieldObjects:
		field = Field.fieldObjects[field]
		if x >= field.x and x <= field.x + field.width:
			if y >= field.y and y <= field.y + field.height:
				return field
	return border

@window.event
def on_mouse_press(x, y, button, modifiers):
	if button == pyglet.window.mouse.LEFT:
		global object_under
		object_pressed = object_under
		object_pressed.select()


@window.event
def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
	if buttons & pyglet.window.mouse.LEFT:
		if Button.selected:
			global object_under
			object_dragged = get_object(x, y)
			object_under = object_dragged
			if object_dragged.__class__.__name__ == "Field":
				object_dragged.select()
				if object_dragged.type != "border":
					if Button.selected.type in field_combinations[object_dragged.type]:
						color = colors["green"]
					else:
						color = colors["red"]

					if Field.highlighted_field is not None:
						if not object_dragged.is_highlighted():
							Field.highlighted_field.stop_highlight()
							object_dragged.start_highlight(color)
					else:
						object_dragged.start_highlight(color)

@window.event
def on_mouse_motion(x, y, dx, dy):
	global object_under
	object_under = get_object(x, y)
	if Button.selected:
		if object_under.__class__.__name__ == "Field":
			if object_under.type != "border":
				if Button.selected.type in field_combinations[object_under.type]:
					color = colors["green"]
				else:
					color = colors["red"]
				if Field.highlighted_field is not None:
					if not object_under.is_highlighted():
						Field.highlighted_field.stop_highlight()
						object_under.start_highlight(color)
				else:
					object_under.start_highlight(color)

@window.event
def on_draw():
	window.clear()
	batch.draw()
	fps_display.draw()

g_area = GameArea()
m_area = MenuArea()
border = Border()

pyglet.app.run()