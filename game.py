import pyglet, pyglet.gl, time, math
from objects import *


class Game(object):
	def __init__(self, window_height, window_width):
		self.field_size = 50
		self.game_area = GameArea(window_height, window_width, self.field_size, self)
		self.resource_area = ResourceArea(window_height, window_width, self.field_size)
		self.production_stockpile_area = ProductionStockpileArea(self, window_height, window_width)
		self.menu_area = MenuArea(self, window_height, window_width, self.field_size)
		
		self.selectable_objects = list(self.menu_area.children)
		self.selected_item = None
		
		self.resource_buildings = []
		self.vehicles = []
		
		self.resource_update_clock = 1/10.0
		
		pyglet.clock.schedule_interval(self.resources_update, self.resource_update_clock)
	
	def compensate_shift_in_border(self, shift_x, shift_y):
		self.resource_area.change_position(shift_x, shift_y)
		self.production_stockpile_area.change_position(shift_x, shift_y)
		self.menu_area.change_position(shift_x, shift_y)
		
	def resources_update(self, dt):
		self.production_stockpile_area.update_stockpile_labels_resources()
	
	def add_to_selectable_objects(self, object):
		self.selectable_objects.append(object)
	
	def add_to_resource_buildings(self, object):
		self.resource_buildings.append(object)

	def get_object(self, x, y, shift_x, shift_y):
		#menu area
		if x+shift_x >= self.menu_area.x and x+shift_x <= self.menu_area.x + self.menu_area.width and y+shift_y >= self.menu_area.y and y+shift_y <= self.menu_area.y + self.menu_area.height:
			return self.menu_area.get_button(x+shift_x, y+shift_y)
		#game area
		elif x+shift_x >= self.game_area.x and x+shift_x <= self.game_area.x + self.game_area.width and y+shift_y >= self.game_area.y and y+shift_y <= self.game_area.y + self.game_area.height:
			field = self.game_area.get_field(x+shift_x, y+shift_y)
			if field is not None and field.building_present is not None:
				return field.building_present
			else:
				return field
		#borders
		else:
			return None
	
	def get_multiple_fields(self, x, y, shift_x_total, shift_y_total):
		return self.game_area.get_fields(x+shift_x_total, y+shift_y_total)
	
	def select_item(self, item):
		if item in self.selectable_objects:
			if self.selected_item is None:
				self.selected_item = item
				self.selected_item.select()
			elif self.selected_item == item:
				self.selected_item.deselect()
				self.selected_item = None
			else:
				self.selected_item.deselect()
				self.selected_item = item
				self.selected_item.select()
	
	def deselect_all(self):
		if self.selected_item is not None:
			self.selected_item.deselect()
			self.selected_item = None
	
	def show_selected_fields_resources(self, fields):
		resources = [0, 0, 0, 0] #wood, stone, iron, food
		for field in fields[1]:
			amounts = field.show_resources()
			resources[0] += amounts[0]
			resources[1] += amounts[1]
			resources[2] += amounts[2]
			resources[3] += amounts[3]
		self.resource_area.update_resources(resources)
	
	def update_selected_fields_highligh(self, fields):
		for field in fields[1]:
			field.highlight(fields[1])


class GameArea(object):
	def __init__(self, window_height, window_width, field_size, game_object):
		self.field_size = field_size
		self.game_object = game_object
		self.x = 0
		self.y = 0
		self.create_grid()
		self.height = self.field_size * self.rows
		self.width = self.field_size * self.columns
		self.selection_area = SelectionArea()
	
	def create_grid(self):
		#columns = math.floor(self.width / self.field_size)
		#rows = math.floor(self.height / self.field_size)
		
		self.columns = 20
		self.rows = 20
		
		self.grid = []
		
		for column in range(int(self.columns)):
			self.grid.append([])
			for row in range(int(self.rows)):
				type = field_types[random.randrange(0,len(field_types),1)]
				position_x = self.x+column*self.field_size
				position_y = self.y+row*self.field_size
				if type == "Empty":
					self.grid[column].append(Empty(position_x, position_y, self.game_object))
				elif type == "Forest":
					self.grid[column].append(Forest(position_x, position_y, self.game_object))
				elif type == "Meadow":
					self.grid[column].append(Meadow(position_x, position_y, self.game_object))
				elif type == "Mountain":
					self.grid[column].append(Mountain(position_x, position_y, self.game_object))
				elif type == "Water":
					self.grid[column].append(Water(position_x, position_y, self.game_object))

	def get_field(self, x, y):
		column = int(math.floor((x - self.x) / self.field_size))
		row = int(math.floor((y - self.y) / self.field_size))
		max_column = len(self.grid)-1
		max_row = len(self.grid[0])-1
		if column >= 0 and column <= max_column and row >= 0 and row <= max_row:
			return self.grid[column][row]
	
	def get_fields(self, x, y):
		column = int(math.floor((x - self.x) / self.field_size))
		row = int(math.floor((y - self.y) / self.field_size))
		max_column = len(self.grid)-1
		max_row = len(self.grid[0])-1
		
		if column >= 0 and column <= max_column and row >= 0 and row <= max_row:
			if self.selection_area.get_size() == 1:
				return (self.grid[column][row], [self.grid[column][row]])
			elif self.selection_area.get_size() == 9:
				fields = []
				if column-1 >= 0 and row-1 >= 0:
					fields.append(self.grid[column-1][row-1])
				if row-1 >= 0:
					fields.append(self.grid[column][row-1])
				if column+1 <= max_column and row-1 >= 0:
					fields.append(self.grid[column+1][row-1])
				if column-1 >= 0:
					fields.append(self.grid[column-1][row])
				fields.append(self.grid[column][row])
				if column+1 <= max_column:
					fields.append(self.grid[column+1][row])
				if column-1 >= 0 and row+1 <= max_row:
					fields.append(self.grid[column-1][row+1])
				if row+1 <= max_row:
					fields.append(self.grid[column][row+1])
				if column+1 <= max_column and row+1 <= max_row:
					fields.append(self.grid[column+1][row+1])
				return (self.grid[column][row], fields) #vraci (kliknute pole, [seznam poli ve vyberu])
		else:
			return (None ,[])

class MenuArea(object):
	def __init__(self, game, window_height, window_width, field_size):
		self.field_size = field_size
		self.height = window_height * 0.74
		self.width = window_width * 0.2
		#anchors jsou vazane na gamearea height a width
		self.x = window_width * 0.8
		self.y = window_height * 0.2
		
		self.game_object = game
		self.image = pyglet.resource.image("border1.png")
		self.image.width = self.width
		self.image.height = self.height
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch, group=foreground)
		
		self.children = []
		
		self.children.append(ResourceLabel("Buildings:", 20, self.x + 20, self.y + ((self.height / 8) * 7), (255,0,0,255), True))
		self.children.append(ButtonBuildingFactoryWood(self.x + ((self.width/8) * 1), self.y+(self.height/2), self.game_object))
		self.children.append(ButtonBuildingFactoryStone(self.x + ((self.width/8) * 3), self.y+(self.height/2), self.game_object))
		self.children.append(ButtonBuildingFactoryIron(self.x + ((self.width/8) * 5), self.y+(self.height/2), self.game_object))
	
	def change_position(self, shift_x, shift_y):
		self.x -= shift_x
		self.y -= shift_y
		if self.sprite is not None:
			self.sprite.x = self.x
			self.sprite.y = self.y
		for child in self.children:
			child.change_position(shift_x, shift_y)
	
	def get_button(self, x, y):
		for child in self.children:
			if child.type == "button":
				if x >= child.x and x <= child.x + child.width and y >= child.y and y <= child.y + child.height:
					return child
	
	def select_button(self, button):
		self.selected_button = button

class ProductionStockpileArea(object):
	def __init__(self, game_object, window_height, window_width):
		self.height = window_height * 0.06
		self.width = window_width
		self.x = 0
		self.y = window_height * 0.94
		
		self.rect = (self.x, self.y, self.x+self.width, self.y, self.x+self.width, self.y+self.height, self.x, self.y+self.height)
		self.game_object = game_object
		self.stockpile =	{"Wood":0,
							"Stone":0,
							"Iron":0}
		self.production =	{"Wood":0,
							"Stone":0,
							"Iron":0}
		
		self.image = pyglet.resource.image("border3.png")
		self.image.width = self.width
		self.image.height = self.height
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch, group=foreground)
		
		self.label_pro_wood = ResourceLabel("Wood: 0 +0", 16, self.x + ((self.width / 4) * 0), self.y, (0,255,0,255), True)
		self.label_pro_stone = ResourceLabel("Stone: 0 +0", 16, self.x + ((self.width / 4) * 1), self.y, (100,100,100,255), True)
		self.label_pro_iron = ResourceLabel("Iron: 0 +0", 16, self.x + ((self.width / 4) * 2), self.y, (255,255,255,255), True)
		self.label_pro_food = ResourceLabel("Food: 0 +0", 16, self.x + ((self.width / 4) * 3), self.y, (255,128,0,255), True)

		self.children = []
		self.children.append(self.label_pro_wood)
		self.children.append(self.label_pro_stone)
		self.children.append(self.label_pro_iron)
		self.children.append(self.label_pro_food)
		
	def reset_production(self):
		self.production =	{"Wood":[0, 0.0],
							"Stone":[0, 0.0],
							"Iron":[0, 0.0]}
	
	def change_position(self, shift_x, shift_y):
		self.x -= shift_x
		self.y -= shift_y
		if self.sprite is not None:
			self.sprite.x = self.x
			self.sprite.y = self.y
		for child in self.children:
			if child.type == "label":
				child.x -= shift_x
				child.y -= shift_y
			elif child.type == "button":
				child.sprite.x -= shift_x
				child.sprite.y -= shift_y

	def update_stockpile_labels_resources(self):
		self.reset_production()
		for res_building in self.game_object.resource_buildings:
			partial_production = res_building.production * (self.game_object.resource_update_clock / 60)
			res_building.update_production()
			res_building.harvest_resources(partial_production)
			self.production[res_building.resource][0] += res_building.production #production per minute
			self.production[res_building.resource][1] += partial_production #production per update tick
			
		self.stockpile["Wood"] += self.production["Wood"][1]
		self.stockpile["Stone"] += self.production["Stone"][1]
		self.stockpile["Iron"] += self.production["Iron"][1]
		
		self.label_pro_wood.text = "Wood: %i +%i/min" % (self.stockpile["Wood"], self.production["Wood"][0]) #wood_amount
		self.label_pro_stone.text = "Stone: %i +%i/min" % (self.stockpile["Stone"], self.production["Stone"][0]) #stone_amount
		self.label_pro_iron.text = "Iron: %i +%i/min" % (self.stockpile["Iron"], self.production["Iron"][0]) #iron_amount


class ResourceArea(object):
	def __init__(self, window_height, window_width, field_size):
		self.field_size = field_size
		self.height = window_height * 0.2
		self.width = window_width * 0.2
		#anchors jsou vazane na gamearea height a width
		self.x = window_width * 0.8
		self.y = 0
		
		self.image = pyglet.resource.image("border2.png")
		self.image.width = self.width
		self.image.height = self.height
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch, group=foreground)
		
		self.children = []
		
		self.label_resources = ResourceLabel("Resources:", 20, self.x + 20, self.y + ((self.height / 5) * 4.5), (255,0,0,255), True)
		self.label_res_wood = ResourceLabel("Wood: 0", 16, self.x + 20, self.y + ((self.height / 5) * 3.5), (0,255,0,255), False)
		self.label_res_stone = ResourceLabel("Stone: 0", 16, self.x + 20, self.y + ((self.height / 5) * 2.5), (100,100,100,255), False)
		self.label_res_iron = ResourceLabel("Iron: 0", 16, self.x + 20, self.y + ((self.height / 5) * 1.5), (255,255,255,255), False)
		self.label_res_food = ResourceLabel("Food: 0", 16, self.x + 20, self.y + ((self.height / 5) * 0.5), (255,128,0,255), False)
		
		self.children.append(self.label_resources)
		self.children.append(self.label_res_wood)
		self.children.append(self.label_res_stone)
		self.children.append(self.label_res_iron)
		self.children.append(self.label_res_food)

	def change_position(self, shift_x, shift_y):
		self.x -= shift_x
		self.y -= shift_y
		if self.sprite is not None:
			self.sprite.x = self.x
			self.sprite.y = self.y
		for child in self.children:
			if child.type == "label":
				child.x -= shift_x
				child.y -= shift_y
			elif child.type == "button":
				child.sprite.x -= shift_x
				child.sprite.y -= shift_y
	
	def update_resources(self, amounts):
		self.label_res_wood.text = "Wood: %i" % amounts[0] #wood_amount
		self.label_res_stone.text = "Stone: %i" % amounts[1] #stone_amount
		self.label_res_iron.text = "Iron: %i" % amounts[2] #iron_amount
		self.label_res_food.text = "Food: %i" % amounts[3] #iron_amount
		